
s
INSERT INTO cliente (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Maria', 'a@gmail.com', '123456789-1', '070501', 'Rua Rua', '99999-111');
INSERT INTO cliente (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Joao', 'a@gmail.com', '123456789-2', '070501', 'Rua Rua', '99999-111');
INSERT INTO cliente (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Jose', 'a@gmail.com', '123456789-3', '070501', 'Rua Rua', '99999-111');
INSERT INTO cliente (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Pedro', 'a@gmail.com', '123456789-4', '070501', 'Rua Rua', '99999-111');


INSERT INTO usuario (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Mateus', 'a@gmail.com', '12345678-01', '070501', 'Rua Rua', '99999-111');
INSERT INTO usuario (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Ana', 'a@gmail.com', '12345678-02', '070501', 'Rua Rua', '99999-111');
INSERT INTO usuario (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Juca', 'a@gmail.com', '12345678-03', '070501', 'Rua Rua', '99999-111');
INSERT INTO usuario (senha, nome, email, cpf, nascimento, endereco, celular) VALUES ( '12345678', 'Jesus', 'a@gmail.com', '12345678-04', '070501', 'Rua Rua', '99999-111');


INSERT INTO categoria (nome,descricao, imagem) VALUES ("Ficção", "Livro de ficção", "/imagem");
INSERT INTO categoria (nome,descricao, imagem) VALUES ("Aventura", "Livro de Aventura", "/imagem");
INSERT INTO categoria (nome,descricao, imagem) VALUES ("Romance", "Livro de Romance", "/imagem");
INSERT INTO categoria (nome,descricao, imagem) VALUES ("Economia", "Livro de Economia", "/imagem");


INSERT INTO livro (idCategoria, imagem, titulo, autor, precoAntigo, precoNovo, paginas) VALUES (1,"/imagem" , "Livro A" , "Autor A" ,22.5 , 22.5 , 200);
INSERT INTO livro (idCategoria, imagem, titulo, autor, precoAntigo, precoNovo, paginas) VALUES (2,"/imagem" , "Livro B" , "Autor B" ,22.5 , 22.5, 200);
INSERT INTO livro (idCategoria, imagem, titulo, autor, precoAntigo, precoNovo, paginas) VALUES (3,"/imagem" , "Livro C" , "Autor C" ,22.5 , 22.5 , 200);
INSERT INTO livro (idCategoria, imagem, titulo, autor, precoAntigo, precoNovo, paginas) VALUES (4,"/imagem" , "Livro D" , "Autor D" ,22.5 , 22.5 , 200);

INSERT INTO compra ( quando , total , idCliente ) VALUES (22022018 , 100 , 1 );
INSERT INTO compra ( quando , total , idCliente ) VALUES (22022018 , 100 , 2 );
INSERT INTO compra ( quando , total , idCliente ) VALUES (22022018 , 100 , 3 );
INSERT INTO compra ( quando , total , idCliente ) VALUES (22022018 , 100 , 4 );


INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (1 , 1 , 2 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (2 , 1 , 3 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (3 , 2 , 2 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (4 , 2 , 3 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (1 , 3 , 4 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (3 , 3 , 5 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (2 , 4 , 2 , 22.51);
INSERT INTO item (idLivro , idCompra, quantidade , preco) VALUES (4 , 4 , 3 , 22.51);